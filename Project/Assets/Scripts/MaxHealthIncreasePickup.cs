﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Networking;

public class MaxHealthIncreasePickup : MonoBehaviour
{
    // Use this for initialization
    
    public int _healtInc = 10;
    public int _rotSpeed = 20;

    public void Update()
    {
            transform.Rotate(Vector3.up, _rotSpeed * Time.deltaTime);
    }

    public void ActivatePower(PlayerStats stats)
    {
        stats.Health += _healtInc;
    }

    public void DestroyPowerUp()
    {
        Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            ActivatePower(col.gameObject.GetComponent<PlayerStats>());
            DestroyPowerUp();
        }
    }
}