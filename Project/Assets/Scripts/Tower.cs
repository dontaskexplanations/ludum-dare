﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Tower : NetworkBehaviour {

    //Variables
    public GameObject _ProjectileOriginal;
    private float _IdleTimeRemaining;
    private TowerStats _Stats;
    private GameObject _Target = null;
    private List<GameObject> _EnemiesInView = new List<GameObject>();

	// Use this for initialization
	void Start () {
        _Stats = gameObject.GetComponent<TowerStats>();
        _IdleTimeRemaining = _Stats.FireInterval;
	}
	
	// Update is called once per frame
	void Update () {
        _IdleTimeRemaining -= Time.deltaTime;
        if (_Target == null && _EnemiesInView.Count > 0)
        {
            bool areMinionsInView = false;
            foreach (GameObject obj in _EnemiesInView)
            {
                if (!obj.CompareTag("Player"))
                {
                    areMinionsInView = true;
                    break;
                }
            }

            while (_Target == null)
            {
                int idx = Random.Range(0, _EnemiesInView.Count);
                if (!areMinionsInView)
                {
                    _Target = _EnemiesInView[idx];
                }
                else if (!_EnemiesInView[idx].CompareTag("Player"))
                {
                    _Target = _EnemiesInView[idx];
                }
            }
        }
        else if (_Target != null && _IdleTimeRemaining <= 0.0f)
        {
            FireProjectile(_Target);
            _IdleTimeRemaining = _Stats.FireInterval;
        }
    }

    void FireProjectile(GameObject target)
    {
        GameObject projectile = Instantiate(_ProjectileOriginal,_ProjectileOriginal.transform.position,_ProjectileOriginal.transform.rotation);
        projectile.SetActive(true);
        projectile.GetComponent<TowerProjectile>().SetProjectileSpeed(_Stats.ProjectileSpeed);
        projectile.GetComponent<TowerProjectile>().Fire(target);
    }

    private void OnTriggerEnter(Collider other)
    {
            TeamComponent spotted = other.gameObject.GetComponent<TeamComponent>();
            if (spotted != null && GetComponent<TeamComponent>().IsInBlueTeam != other.gameObject.GetComponent<TeamComponent>().IsInBlueTeam)
            {
            _EnemiesInView.Add(other.gameObject);
            }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == _Target) _Target = null;
        _EnemiesInView.Remove(other.gameObject);
    }
}
