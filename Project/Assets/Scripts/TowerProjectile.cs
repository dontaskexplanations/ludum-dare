﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TowerProjectile : NetworkBehaviour {

    //Variables
    private float _ProjectileSpeed;
    private GameObject _Target;
    private bool _IsFired = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (_IsFired) MoveTowardsTarget();
	}

    public void Fire(GameObject target)
    {
        _Target = target;
        _IsFired = true;
    }

    void MoveTowardsTarget()
    {
        Vector3 dir = _Target.transform.position - transform.position;
        dir = Vector3.Normalize(dir);
        dir *= _ProjectileSpeed * Time.deltaTime;
        transform.Translate(dir);
    }

    public void SetProjectileSpeed(float speed)
    {
        _ProjectileSpeed = speed;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == _Target)
        {
            Destroy(gameObject);
        }
    }
}
