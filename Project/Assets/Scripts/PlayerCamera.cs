﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {

    public Camera PlayerCam;
    public GameObject Player;

    //publics
    public float Speed, RotationSpeed;
    public Transform Cam;
    public GameObject CharControll;
    public Vector2 YLimit;
    public GunScript Gun;

    private bool IsActivated = true;
    //privates
    private Vector3 _vel;
    private float _clamp;

    // Use this for initialization
    void Start ()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (IsActivated)
        {

        if (!CharControll) return;
            //transform.eulerAngles = new Vector3(0, yRotation, 0);
            CharControll.GetComponent<CharacterMove>().CmdRotate(new Vector3(0, Input.GetAxis("Mouse X") * RotationSpeed * Time.deltaTime, 0)) ;

            //player vertical rotatie
            _clamp += Input.GetAxis("Mouse Y") * RotationSpeed * Time.deltaTime;    //om de input op te slaan die we krijgen
            _clamp = Mathf.Clamp(_clamp, YLimit.x, YLimit.y);   //om de rotatie vast te zetten in een clamp
            Cam.eulerAngles = new Vector3(-_clamp, CharControll.transform.eulerAngles.y, CharControll.transform.eulerAngles.z);
            transform.position = CharControll.transform.position;
        }
    }

    public void SetActiveBool(bool state)
    {
        IsActivated = state;
    }

}
