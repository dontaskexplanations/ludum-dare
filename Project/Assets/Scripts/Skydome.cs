﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skydome : MonoBehaviour {


	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerExit(Collider other)
    {
      
        other.gameObject.GetComponent<CharacterMove>().moveDirection= -other.gameObject.GetComponent<CharacterMove>().moveDirection;
        other.gameObject.GetComponent<PlayerStats>().JetPackFuel = 0;
    }
}
