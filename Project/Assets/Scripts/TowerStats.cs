﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TowerStats : NetworkBehaviour
{

    //Variables
    [SyncVar]
    [SerializeField] private float _MaxHealth;
    [SyncVar]
    [SerializeField] private float _FireInterval;
    [SyncVar]
    [SerializeField] private float _ProjectileSpeed;
    [SyncVar]
    [SerializeField] private float _ProjectileDamage;
    [SyncVar]
    [SerializeField] private float _Health;

    public void Start()
    {
        _Health = _MaxHealth;
    }

    public float Health
    {
        get { return _Health; }
        set { _Health = value; }
    }

    public float MaxHealth
    {
        get
        {
            return _MaxHealth;
        }
        set
        {
            _MaxHealth = value;
        }
    }

    public float FireInterval
    {
        get
        {
            return _FireInterval;
        }
        set
        {
            _FireInterval = value;
        }
    }

    public float ProjectileSpeed
    {
        get
        {
            return _ProjectileSpeed;
        }
        set
        {
            _ProjectileSpeed = value;
        }
    }

    public float ProjectileDamage
    {
        get
        {
            return _ProjectileDamage;
        }
        set
        {
            _ProjectileDamage = value;
        }
    }
}