﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GamepadInput;

public class InputBehaviour : MonoBehaviour
{
    public void Initilize(GamePad.Index index)
    {
        InputControler.Instance.AddInput(this,(int)index);
    }

    virtual public void OnMove(Vector2 axis) { }
    virtual public void OnJump() { }
    virtual public void OnMouseLeftClick(Vector2 mousePos) { }
}