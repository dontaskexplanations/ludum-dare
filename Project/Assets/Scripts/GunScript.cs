﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GunScript : NetworkBehaviour
{

    public GameObject LineObject;
    public GameObject player;
    public PlayerStats playerStatistics;
    
    private LineRenderer _Line;
    private MinionStats enemy;

    public bool CanShoot = true;
    public float ShootCounterMax = 0.1f;
    private float ShootCounter;

    [SerializeField] private Transform _Start;

    // Use this for initialization
    void Start()
    {
        _Line = LineObject.GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        if (isClient)
        {
            UpdateShootCounter();
            _Start = player.transform;

            if(CanShoot)HandleRaycastAndPointer();
           
        }
    }

    void HandleRaycastAndPointer()
    {
        RaycastHit hit;
        float distance = 1000.0f;
        //if (Physics.Raycast(_Start.position, transform.parent.forward, out hit,5000.0f, 0 , QueryTriggerInteraction.Ignore))
        if (Physics.Raycast(_Start.position, transform.parent.forward, out hit))
        {
            Collider target = hit.collider; // What did I hit?
            distance = hit.distance; // How far out?
            Vector3 _Location = hit.point; // Where did I make impact?
            GameObject targetGameObject = hit.collider.gameObject; // What's the GameObject?

            if (Input.GetMouseButton(0) && playerStatistics.Ammo > 0) // primary fire button
            {
                playerStatistics.Ammo -= 1;
                CanShoot = false;
                // implementing damage

                if (hit.collider.gameObject.GetComponent<MinionStats>() != null)
                {
                    player.GetComponent<CharacterMove>().CmdDealDamage(hit.collider.gameObject,1);
                }

            }

        }
        else if (Input.GetMouseButton(0) && playerStatistics.Ammo > 0) // fired in the air, without collision
        {
            playerStatistics.Ammo -= 1;
            CanShoot = false;
        }
    }

    void UpdateShootCounter()
    {
        if (CanShoot == false)
        {
            ShootCounter += Time.deltaTime;
            if (ShootCounter > ShootCounterMax)
            {
                CanShoot = true;
                ShootCounter = 0;
            }

        }
    }

}
