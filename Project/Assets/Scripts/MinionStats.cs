﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class MinionStats : NetworkBehaviour {
    
    public Image HealthBarBase;
    public Image HealthBarRed;

    public Color HealthColor = Color.red;
    public Color HealthBarColor = Color.black;

    private Transform _Tr;
    public GameObject HealthBar;

    [SyncVar]
    private float _health;
    [SerializeField]
    private float _maxHealth;
    [SerializeField]
    private float _damage;

    // Use this for initialization

    public float health
    {
        get { return _health; }
        set { _health = value; }
    }

    public float maxHealth
    {
        get { return _maxHealth; }
        set { _maxHealth = value; }
    }

    public float damage
    {
        get { return _damage; }
        set { _damage = value; }
    }
    void Start()
    {
        _Tr = GetComponent<Transform>();
        health = maxHealth;
    }
    
    void Update()
    {
        // transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.up);
        // var wantedPos = Camera.main.WorldToViewportPoint(_Tr.position);
        // transform.position = wantedPos;
    }
    
    public void DealDamage(float damage, GameObject damagingObject)
    {
        _health -= damage;
        Debug.Log(_health);

        RpcDealDamage(_health);
        if (_health < 1)
        {
            if (gameObject.tag == "Player")
            {
                gameObject.GetComponent<CharacterMove>().Respawn();
            }
            else
            {
                damagingObject.GetComponent<PlayerStats>().Money += 100; // placeholder 100
                damagingObject.GetComponent<CharacterMove>().RpcMoney(damagingObject,damagingObject.GetComponent<PlayerStats>().Money);

                DestroyObject(gameObject);
            }
        }
    }

    [ClientRpc]
    public void RpcDealDamage( float health)
    {
        _health = health;
        
        HealthBarRed.fillAmount = _health * (1.0f / _maxHealth);

        if(gameObject.tag == "Player")
        {
            GetComponent<PlayerUI>().hpTimerCounter = 0;
            GetComponent<PlayerUI>().hasBeenHit = true;
        }
    }
}
