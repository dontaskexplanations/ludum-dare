﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Shop : NetworkBehaviour
{
    private GameObject _Player;

    public GameObject _Tower;

    public float _BuildCost = 1000.0f;

    public bool _IsUpgradable = true;

    public int []_arrCosts= new int[4];

    public int _CurrentLevel=0;
    // Use this for initialization
    void Start()
    {
        _Player = GameObject.FindWithTag("Player");
        
        for (int i = 0; i < _arrCosts.Length; i++)
        {
            _arrCosts[i] = (int)(1000 * Mathf.Pow(2, i));
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collisionInfo)
    {
        if (collisionInfo.gameObject.CompareTag("Player"))
        {
            _Player = collisionInfo.gameObject;
            GameObject.Find("UI").GetComponent<UIScript>().HandleShopUI(true);
        }
    }

    private void OnTriggerExit(Collider collisionInfo)
    {
        if (collisionInfo.gameObject.CompareTag("Player"))
        {
            _Player = collisionInfo.gameObject;
            GameObject.Find("UI").GetComponent<UIScript>().HandleShopUI(false);

        }
    }

    void OnTriggerStay(Collider collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Player") {
            _Player = collisionInfo.gameObject;
            if (_IsUpgradable && _Player.GetComponent<PlayerStats>().Money >= _arrCosts[_CurrentLevel])
            {
                if (_IsUpgradable)
                {
                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        _Player.GetComponent<CharacterMove>().CmdUpgradeTower(gameObject);
                    }
                }
            }
        }

    }
}
