﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.Networking;

public class PlayerUI : NetworkBehaviour
{

    public GameObject player;
    public CharacterController PlayerController;
    public Canvas UI_Canvas;
    private UIScript UI_Script;
    PlayerStats Player_Stat;
    MinionStats Minion_Stat;

    //Sliders

    //private Rigidbody playerRb;

    public bool hasBeenHit = false;
    public bool hasBeenFlying = false;

    // Time control vars
    public float hpTimerCounter = 0; // HealthPointsTimer
    public float hpMaxTimer = 2; // HealthPointsMaxTimer

    public float jpTimerCounter = 0; // JetPackTimer
    public float jpMaxTimer = 2; // JetPackMaxTimer

    // Use this for initialization
    void Start ()
    {

    }

    // Update is called once per frame
    private void LateUpdate()
    {
        if (Input.GetButton("Jump"))
        {
            hasBeenFlying = true;
            jpTimerCounter = 0;
        }
    }

    void Update ()
    {
        if (!isLocalPlayer) return;

        //player_UI.UI_Canvas = GameObject.Find("UI").GetComponent<Canvas>();
        if (!UI_Script) UI_Script = GameObject.Find("UI").GetComponent<UIScript>();
        if(!Player_Stat) Player_Stat = GetComponent<PlayerStats>();
        if (!Minion_Stat) Minion_Stat = GetComponent<MinionStats>();
        if (!PlayerController) PlayerController = GetComponent<CharacterController>();
        Player_Stat.Health = Minion_Stat.health;

        if (Input.GetKey(KeyCode.R)) // testing code 
        {
            Player_Stat.Ammo = 64.0f;
        }

        UI_Script.UpdateHealthBar(Player_Stat.Health,Player_Stat.Health);
        UI_Script.UpdateAmmoBar(Player_Stat.Ammo,Player_Stat.Ammo);
        UI_Script.UpdateJetpackBar(Player_Stat.JetPackFuel, Player_Stat.JetPackFuel);
        UI_Script.UpdateMoney(Player_Stat.Money);

        UI_Script.HandleDangerIndicator();
        UI_Script.HandlePowerups();

        HandleTime();
        handleRegen();

    }

    void HandleTime() // handle the timers 
    {
        if (hasBeenHit) // if the player has been hit, start counter for regen
        {
            hpTimerCounter += Time.deltaTime;
            if (hpTimerCounter > hpMaxTimer)
            {
                hasBeenHit = false;
                hpTimerCounter = 0;
            }
        }

        if (hasBeenFlying)
        {
            if (PlayerController.isGrounded)
            {
                hasBeenFlying = false;
            }
        }
    }

    void handleRegen() // handle regen when player is not hit or isnt flying
    {
        if (hasBeenHit == false)
        {
            if (Minion_Stat.health < Minion_Stat.maxHealth)
            {
                player.GetComponent<CharacterMove>().CmdDealDamage(gameObject, -Player_Stat.HealthRegeneration);
            }
        }

        if (hasBeenFlying == false)
        {

            if (Player_Stat.JetPackFuel < Player_Stat.MaxJetPackFuel)
            {
                Player_Stat.JetPackFuel += Player_Stat.RefuelingAmount;
            }
        }
    }
}

