﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkydomeWarningScript : MonoBehaviour {
    private GameObject Player;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerExit(Collider collisionInfo)
    {
        if (collisionInfo.gameObject.CompareTag("Player"))
        {
            Player = collisionInfo.gameObject;
            GameObject.Find("UI").GetComponent<UIScript>().IsCloseToBorder = true;

        }
    }

    private void OnTriggerEnter(Collider collisionInfo)
    {
        if (collisionInfo.gameObject.CompareTag("Player"))
        {
            Player = collisionInfo.gameObject;
            GameObject.Find("UI").GetComponent<UIScript>().IsCloseToBorder = false;

        }
    }
}
