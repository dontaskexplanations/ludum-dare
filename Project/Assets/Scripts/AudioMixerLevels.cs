﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioMixerLevels : MonoBehaviour {

    public AudioMixer _Mixer;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetMasterVolume(float vol)
    {
        _Mixer.SetFloat("masterVol", vol);
    }

    public void SetMusicVolume(float vol)
    {
        _Mixer.SetFloat("musicVol", vol);
    }

    public void SetSFXVolume(float vol)
    {
        _Mixer.SetFloat("sfxVol", vol);
    }

    public void SetAmbientVolume(float vol)
    {
        _Mixer.SetFloat("ambientVol", vol);
    }
}
