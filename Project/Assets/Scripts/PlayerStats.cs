﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerStats : NetworkBehaviour
{
    [SerializeField] private float _HealthRegenRate;

    [SyncVar]
    [SerializeField] private float _Health;

    [SerializeField] private float _MaxHealth;

    [SerializeField] private float _Ammo;

    [SerializeField] private float _MaxAmmo;

    [SerializeField] private float _JetPackFuel;

    [SerializeField] private float _MaxJetPackFuel;

    [SerializeField] private float _RefuelingAmount;

    [SyncVar]
    [SerializeField] private float _Money;
    
    //Hi timo, papy here ... cleaned ur code a bit ;)
    public float Health
    {
        get { return _Health; }
        set { _Health = value; }
    }

    public float Ammo
    {
        get { return _Ammo; }
        set { _Ammo = value; }
    }

    public float JetPackFuel
    {
        get { return _JetPackFuel; }
        set { _JetPackFuel = value; }
    }


    public float MaxHealth
    {
        get { return _MaxHealth; }
        set { _MaxHealth = value; }
    }

    public float MaxAmmo
    {
        get { return _MaxAmmo; }
        set { _MaxAmmo = value; }
    }

    public float MaxJetPackFuel
    {
        get { return _MaxJetPackFuel; }
        set { _MaxJetPackFuel = value; }
    }
    public float HealthRegeneration
    {
        get { return _HealthRegenRate; }
        set { _HealthRegenRate = value; }
    }

    public float RefuelingAmount
    {
        get { return _RefuelingAmount; }
        set { _RefuelingAmount = value; }
    }

    public float Money
    {
        get { return _Money; }
        set { _Money = value; }
    }

}