﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SharkSpawn : NetworkBehaviour
{
    public float SpawnInterval;
    public float DistanceFromSpawn;
    public GameObject Shark;
    public int GroupSize;
    public float GroupSpawnInterval;

    private float _SpawnTimer;
    private float _GroupSpawnTimer;
    private int _GroupCount;
    private bool _CanSpawn = true;
    private bool _IsInBlueTeam;
    public List<GameObject> _Lanes;

	// Use this for initialization
	void Start ()
	{
	    _CanSpawn = true;
	    _IsInBlueTeam = GetComponent<TeamComponent>().IsInBlueTeam;
        _Lanes = new List<GameObject>();
	    for (int i = 0; i < gameObject.transform.childCount; ++i)
	    {
            _Lanes.Add(gameObject.transform.GetChild(i).gameObject);
	    }
        _SpawnTimer = SpawnInterval;

    }
	
	// Update is called once per frame
	void Update ()
	{
        if(isServer == false)
        {
            return;
        }

	    if (!_CanSpawn)
	    {
	        _GroupSpawnTimer += Time.deltaTime;
	        if (_GroupSpawnTimer >= GroupSpawnInterval)
	        {
	            _CanSpawn = true;
	            _GroupSpawnTimer -= GroupSpawnInterval;
	        }
	    }
       
	    if (_CanSpawn)
	    {
	        _SpawnTimer += Time.deltaTime;
	        if (_SpawnTimer >= SpawnInterval)
	        {
	            _SpawnTimer -= SpawnInterval;
	            for(int i = 0; i < _Lanes.Count; ++i)
	            {
	                GameObject newShark = Instantiate(Shark, _Lanes[i].transform.position, Quaternion.identity);
                    NetworkServer.Spawn(newShark);
                    newShark.name = i + " " + _IsInBlueTeam;
                    RpcShark(newShark,i);
                    ++_GroupCount;
	            }
	        }

	        if (_GroupCount == _Lanes.Count * GroupSize)
	        {
	            _CanSpawn = false;
	            _GroupCount = 0;
	        }
	    }
	}

    [ClientRpc]
    public void RpcShark(GameObject obj, int i)
    {
        obj.GetComponent<TeamComponent>().IsInBlueTeam = _IsInBlueTeam;
        obj.GetComponent<Rigidbody>().transform.rotation = _Lanes[i].transform.rotation;
        obj.GetComponent<SharkController>().Lane = _Lanes[i];
    }
}
