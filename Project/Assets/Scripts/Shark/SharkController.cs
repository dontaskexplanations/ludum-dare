﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class SharkController : NetworkBehaviour
{
    public float FOVRadius;
    public float InitialSpeed;
    public float Acceleration;
    public float MaxSpeed;
    public Vector3 Velocity;
    public GameObject Target = null;
    public GameObject Lane;

    private float _Speed;
    private bool _IsInBlueTeam;
    public int _LaneWaypointsPassed = 0;
    private bool isloqded = true;
    private Animator _SharkAnimator;

	// Use this for initialization
	void Start ()
	{
	    _SharkAnimator = GetComponent<Animator>();
	    _IsInBlueTeam = GetComponent<TeamComponent>().IsInBlueTeam;
    }
	
	// Update is called once per frame
	void Update ()
	{
        if (isloqded)
        {
            isloqded = false;
            string[] str = name.Split(' ');
            bool ok = (str[1] == "true");
            int i = 0;
            int.TryParse(str[0], out i);
            GameObject okg = gameObject;
            if (!ok)
            {
                okg = GameObject.FindGameObjectWithTag("Blue");
            }
            else
            {
                okg = GameObject.FindGameObjectWithTag("Red");
            }

            SharkSpawn sh = okg.GetComponent<SharkSpawn>();

            _IsInBlueTeam = ok;
            transform.rotation = sh._Lanes[i].transform.rotation;
            Lane = sh._Lanes[i];
            Target = Lane;
            GetComponent<TeamComponent>().IsInBlueTeam = ok;
        }

        if (GetComponent<MinionStats>().health <= 0)
	    {
	        _SharkAnimator.SetBool("IsDead", true);
            Destroy(gameObject, 1.0f);
	    }

        if (isServer)
        {
            SetTargetPosition();
            GoToTarget();
        }
	}

    void SetTargetPosition()
    {
        GameObject[] towers = GameObject.FindGameObjectsWithTag("Tower");
        GameObject tower = null;
        for (int i = 0; i < towers.Length; ++i)
        {
            if (towers[i].GetComponent<TeamComponent>().IsInBlueTeam != _IsInBlueTeam)
            {
                tower = towers[i];
            }
        }

        GameObject[] enemySharks = GameObject.FindGameObjectsWithTag("Shark");
        GameObject closestEnemyShark = GetClosestGameObject(enemySharks);

        GameObject[] enemyplayers = GameObject.FindGameObjectsWithTag("Player");
        GameObject closestEnemyPlayer = GetClosestGameObject(enemyplayers);

        if (closestEnemyPlayer == null && closestEnemyShark == null)
        {
            if (Lane)
            {
                if (_LaneWaypointsPassed == Lane.transform.childCount)
                    Target = tower;
                else
                    Target = Lane.transform.GetChild(_LaneWaypointsPassed).gameObject;
            }
        }
        else if (closestEnemyShark != null && (Target == null || Target.tag == "Tower" || Target.tag == "Waypoint"))
        {
            Target = closestEnemyShark;
        }
        else if(closestEnemyPlayer != null && (Target == null || Target.tag == "Tower" || Target.tag == "Shark" || Target.tag == "Waypoint"))
        {
            Target = closestEnemyPlayer;
        }
    }

    GameObject GetClosestGameObject(GameObject[] objects)
    {
        GameObject result = null;
        float dist = float.MaxValue;
        foreach (GameObject i in objects)
        {
            if (i == gameObject) continue;

            float tempDist = (i.transform.position - transform.position).magnitude;
            if (i.GetComponent<TeamComponent>())
            {
                if (tempDist < dist && tempDist < FOVRadius && i.GetComponent<TeamComponent>().IsInBlueTeam != _IsInBlueTeam)
                {
                    dist = tempDist;
                    result = i;
                }
            }
        }

        return result;
    }

    void GoToTarget()
    {
        if (!Target) { return; }
        Vector3 direction = Target.transform.position - transform.position;
        float distance = direction.magnitude;
        direction.Normalize();

        _Speed += Acceleration * Time.deltaTime;
        if (_Speed > MaxSpeed) _Speed = MaxSpeed;

        float speedMultiplier = 1.0f;
        Velocity = direction;
        Avoid();
        GetComponent<Rigidbody>().velocity = Velocity.normalized * _Speed * speedMultiplier;

        Quaternion rotation = Quaternion.LookRotation(Target.transform.position - transform.position);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, Time.deltaTime * 150.0f);

        //Update waypoints
        if (_LaneWaypointsPassed < Lane.transform.childCount)
        {
            float distToWaypoint = (Lane.transform.GetChild(_LaneWaypointsPassed).transform.position - transform.position).magnitude;
            Debug.Log(distToWaypoint);
            if (distToWaypoint < FOVRadius)
                ++_LaneWaypointsPassed;
        }
    }

    void Avoid()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit,100))
        {
            Vector3 pos = hit.point;
            GameObject obj = hit.collider.gameObject;
            if(obj.tag != "Tower")
                _Speed = InitialSpeed;
            Vector3 vecToCenter = obj.transform.position - transform.position;
            float distToCenter = vecToCenter.magnitude;
            if ((obj.tag == "Shark" || obj.tag == "Player" || obj.tag == "EditorOnly") && distToCenter < FOVRadius)
            {
                if ((obj.tag == "Shark" || obj.tag == "Player"))
                {
                    if (obj.GetComponent<TeamComponent>().IsInBlueTeam != _IsInBlueTeam)
                    {
                        return;
                    }
                }

                Vector3 dir = GetComponent<Rigidbody>().velocity;
                dir.Normalize();
                float length = Vector3.Dot(dir, vecToCenter);
                Vector3 avoidDir = transform.position + dir * length - obj.transform.position;
                float avoidForce = avoidDir.magnitude;
                if (vecToCenter.normalized == dir)
                {
                    int dirNr = Random.Range(0, 4);
                    if(dirNr == 0)
                        avoidDir = Vector3.up;
                    else if(dirNr == 1)
                        avoidDir = Vector3.down;
                    else if (dirNr == 2)
                        avoidDir = Vector3.right;
                    else
                        avoidDir = Vector3.left;
                    avoidForce = obj.GetComponent<AICollisionSize>().size;
                    avoidDir *= avoidForce;
                    Velocity += avoidDir;
                }
                else if (avoidForce > 0)
                {
                    avoidForce = obj.GetComponent<AICollisionSize>().size / avoidForce;
                    avoidDir.Normalize();
                    avoidDir *= avoidForce;
                    Velocity += avoidDir;
                } 
            }
        }
    }
}

