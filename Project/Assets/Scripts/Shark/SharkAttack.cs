﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SharkAttack : NetworkBehaviour {

    public float AttackDuration;

    //MAKE SURE THIS COOLDOWN IS HIGHER THAN 2, OTHERWISE THE ANIMATIONS WON'T MATCH UP!!!!!!
    public float AttackCooldown;

    private float _AttackTimer;
    private float _CooldownTimer;
    private bool _IsReady;
    private bool _IsCounting;
    private bool _CanHit;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (!_IsReady && !_IsCounting)
	    {
	        _CooldownTimer += Time.deltaTime;
	        if (_CooldownTimer >= AttackCooldown)
	        {
	            _IsReady = true;
	            _CooldownTimer -= AttackCooldown;
	        }
	    }
	    if (_IsCounting)
	    {
	        _AttackTimer += Time.deltaTime;
	        if (_AttackTimer >= AttackDuration)
	        {
	            _CanHit = true;
	            _AttackTimer -= AttackDuration;
	        }
	    }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<TeamComponent>().IsInBlueTeam != GetComponent<TeamComponent>().IsInBlueTeam)
        {
            
        }
    }

   // [ClientCallback]
    void OnTriggerStay(Collider other)
    {
        if (!other.GetComponent<TeamComponent>()) return;
        if (other.GetComponent<TeamComponent>().IsInBlueTeam != GetComponent<TeamComponent>().IsInBlueTeam)
        {
            if (!_IsCounting && _IsReady)
            {
                _IsCounting = true;
                _IsReady = false;
                GetComponent<Animator>().SetBool("IsAttacking", true);
            }
            if (_CanHit)
            {
                _IsCounting = false;
                _CanHit = false;
                other.GetComponent<MinionStats>().DealDamage(GetComponent<MinionStats>().damage);
                GetComponent<Animator>().SetBool("IsAttacking", false);
            }
        }
    }
}
