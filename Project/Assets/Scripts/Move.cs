﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : InputBehaviour {

    private Rigidbody _Rigidbody;
	// Use this for initialization
	public void Start () {
        base.Initilize(GamepadInput.GamePad.Index.Any);
        _Rigidbody = GetComponent<Rigidbody>();

	}

    override public void OnMove(Vector2 axis)
    {
        _Rigidbody.AddForce(new Vector3(axis.x,0,axis.y), ForceMode.VelocityChange);
    }

    override public void OnJump()
    {
        _Rigidbody.AddForce(new Vector3(0,2,0), ForceMode.Impulse);
    }
}
