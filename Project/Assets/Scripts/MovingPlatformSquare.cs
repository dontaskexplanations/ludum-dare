﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class MovingPlatformSquare : MonoBehaviour {

    public Vector3 segVelocity = new Vector3(200, 100, 0);
    private Rigidbody platform;
    private float elapsedSec;
    public float moveTime=5.0f;
    private int segment=0;
	// Use this for initialization
	void Start () {
        platform = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        
        platform.velocity = segVelocity * Time.deltaTime;
        elapsedSec += Time.deltaTime;
        if (elapsedSec > moveTime)
        {
            elapsedSec = 0;
            segment++;
            segment %= 4;
            switch (segment)
            {
                case 0:
                    segVelocity.x = -segVelocity.x;
                    segVelocity.z = -segVelocity.z;
                    break;
                case 1:
                    segVelocity.y = -segVelocity.y;
                    break;
                case 2:
                    segVelocity.x = -segVelocity.x;
                    segVelocity.z = -segVelocity.z;
                    break;
                case 3:
                    segVelocity.y = -segVelocity.y;
                    break;


            }
           

        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        CharacterController body = collision.gameObject.GetComponent<CharacterController>();
        //GameObject collider = collision.gameObject;
        //  body.isKinematic = false;
        body.transform.parent =transform;
        
        //body.isKinematic = true;
        
    }
}
