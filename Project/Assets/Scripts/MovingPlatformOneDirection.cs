﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class MovingPlatformOneDirection : MonoBehaviour {

    private Rigidbody platform;
    public Vector3 velocity = new Vector3(200, 0, 0);
    private float elapsedSec = 0;
    public float moveTime = 5.0f;
    // Use this for initialization
    void Start () {
        platform = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        platform.velocity = velocity * Time.deltaTime;
        elapsedSec += Time.deltaTime;
      
        if (elapsedSec > moveTime)
        {
            velocity = -velocity;
            elapsedSec = 0;
        }

    }
       private void OnCollisionEnter(Collision collision)
        {
            CharacterController body = collision.gameObject.GetComponent<CharacterController>();
            //GameObject collider = collision.gameObject;
            //  body.isKinematic = false;
            body.transform.parent = platform.transform;

            //body.isKinematic = true;

        }
}
