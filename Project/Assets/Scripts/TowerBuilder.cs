﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TowerBuilder : NetworkBehaviour
{
    public GameObject Player;

    [SyncVar]
    public GameObject Tower;

    public float BuildCost = 1000.0f;

    [SyncVar]
    public bool IsBuildable = true;
    [SyncVar]
    public bool IsInBlueTeam = false;
    [SyncVar]
    public GameObject _CreatedTower;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (_CreatedTower == null)
	    {
	        IsBuildable = true;
	    }
	}

    private void OnTriggerEnter(Collider collisionInfo)
    {
        if (collisionInfo.gameObject.CompareTag("Player"))
        {
            Player = collisionInfo.gameObject;
            GameObject.Find("UI").GetComponent<UIScript>().HandleBuildUI(true);
        }
    }

    private void OnTriggerExit(Collider collisionInfo)
    {
        if (collisionInfo.gameObject.CompareTag("Player"))
        {
            Player = collisionInfo.gameObject;
            GameObject.Find("UI").GetComponent<UIScript>().HandleBuildUI(false);

        }   
    }

    void OnTriggerStay(Collider collisionInfo)
    {
        if (collisionInfo.gameObject.CompareTag("Player"))
        {
           Player = collisionInfo.gameObject;

            if (IsBuildable && Player.GetComponent<PlayerStats>().Money >= BuildCost && Player.GetComponent<TeamComponent>().IsInBlueTeam == IsInBlueTeam)
            {
                if (Input.GetKeyDown(KeyCode.F))
                {
                    collisionInfo.gameObject.GetComponent<CharacterMove>().CmdBuildTower(gameObject);
                }
            }
        }
    }

    public void SetCreatedTower(GameObject obj)
    {
        _CreatedTower = obj;
    }
}
