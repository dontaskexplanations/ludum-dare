﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerBehaviour : NetworkBehaviour {

    public float PlayerSpeed;

    private CharacterController _CharCon;

	// Use this for initialization
	void Start () {
        _CharCon = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {

        float horizontalSpeed = Input.GetAxis("Horizontal");
        float verticalSpeed = Input.GetAxis("Vertical");

        // _RB.AddVelocity(horizontalSpeed * PlayerSpeed, 0.0f, verticalSpeed * PlayerSpeed);
        _CharCon.SimpleMove( new Vector3(horizontalSpeed * PlayerSpeed, 0.0f, verticalSpeed * PlayerSpeed));
    }
}
