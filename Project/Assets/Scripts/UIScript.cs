﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public class UIScript : MonoBehaviour {
  
    // Visuals control Vars
    public Image FillHealth;  // assign in the editor the "Fill"
    public Image FillAmmo; // assign in the editor the "Fill"
    public Image FillJetpack; // assign in the editor the "Fill"
    public Text MoneyText;
    public RawImage DeathScreenOverlay;

    public Text BuildTowerText;
    public Text OpenShopText;

    // Indicator Controllers
    public bool IsCloseToBorder;
    public RawImage DangerIndicator;
    public Text DangerText;

    public Image[] CooldownImages = new Image[4];
    
    public bool[] CooldownBooleans = new bool[4];

    public float dangerAlphaInterval = 1;
    private float dangerAlphaIntervalTimer = 0;
    private bool blinkerbool = false;

    private float[] CooldownCounters = new float[4]; // cooldown
    private float[] CooldownMaxCounter = new float[4] { 2,2,2,2}; // cooldown
    public Text[] CooldownTexts = new Text[4];

    //Death vars
    public bool IsDead = false;
    public float DeathCounterMax;
    private float DeathCounter;

    // Use this for initialization
    void Start ()
    {
        DeathScreenOverlay.enabled = false;
        BuildTowerText.enabled = false;
        OpenShopText.enabled = false;
        DangerIndicator.enabled = false;
        DangerText.enabled = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdateCooldownTimer();
        UpdateDangerSign();
        UpdateSkillCooldowns();
        UpdateDeathTimer();
    }

    public void UpdateHealthBar(float Health, float MaxHealth)
    {
        if (Health < MaxHealth / 5)
        {
            FillHealth.fillAmount = 0.1f;
        }
        else
        {
            FillHealth.fillAmount = 1 - (((Health / (float)100) * (float)180 / 360)) * 2;
        }
    }
    public void UpdateAmmoBar(float Ammo, float MaxAmmo)
    {
        FillAmmo.fillAmount =  1 - (((Ammo / (float)100) * (float)180 / 360)) * 3.5f;
    }
    public void UpdateJetpackBar(float JetPackFuel, float MaxJetPackFuel)
    {
        FillJetpack.fillAmount =  1 - ((JetPackFuel / (float)100) * (float)180 / 360);
    }
    public void UpdateMoney(float Money)
    {
        MoneyText.text = Money.ToString();
    }

    void UpdateDangerSign()
    {
        dangerAlphaIntervalTimer += Time.deltaTime;

        if (dangerAlphaIntervalTimer > dangerAlphaInterval)
        {
            blinkerbool = !blinkerbool;
            dangerAlphaIntervalTimer = 0;
        }
    }

    void UpdateCooldownTimer()
    {
        for (int i = 0; i < CooldownBooleans.Length; i++)
        {
            if (CooldownBooleans[i] == true)
            {
                CooldownCounters[i] += Time.deltaTime;
                if (CooldownCounters[i] > CooldownMaxCounter[i])
                {
                    CooldownCounters[i] = 0;
                    CooldownBooleans[i] = false;

                }
            }

        }
    }

    void UpdateSkillCooldowns()
    {
        for (int i = 0; i < CooldownCounters.Length; i++)
        {
            if (CooldownBooleans[i] == true)
            {
                CooldownTexts[i].enabled = true;
                CooldownTexts[i].text = ((int)(CooldownMaxCounter[i]-CooldownCounters[i])).ToString();
            }
            else CooldownTexts[i].enabled = false;

        }
    }

    void UpdateDeathTimer()
    {
        if (IsDead == true)
        {
            DeathScreenOverlay.enabled = true;
            DeathCounter += Time.deltaTime;
            if (DeathCounter > DeathCounterMax)
            {
                IsDead = false;
                DeathCounter = 0;
                DeathScreenOverlay.enabled = false;
            }

        }
        else
        {
            DeathScreenOverlay.enabled = false;
        }

    }


    public void HandleDangerIndicator()
    {
        if (IsCloseToBorder == true)
        {

            if (blinkerbool)
            {
                DangerIndicator.enabled = true;
                DangerText.enabled = true;
            }
            else
            {
                DangerIndicator.enabled = false;
                DangerText.enabled = false;
            }

        }
        else
        {
            DangerIndicator.enabled = false;
            DangerText.enabled = false;
        } 

    }

    public void HandlePowerups()
    {
        for (int i = 0; i < CooldownBooleans.Length; i++)
        {
            if (CooldownBooleans[i] == true) // if it is true, draw the indicator
            {
                CooldownImages[i].enabled = true;
            }
            else
            {
                CooldownImages[i].enabled = false;
            }
        }

    }

    public void HandleShopUI(bool Active)
    {
        OpenShopText.enabled = Active;
    }

    public void HandleBuildUI(bool Active)
    {
        BuildTowerText.enabled = Active;
    }
    public void ChangeCooldownMaxTimer(int index, float newVal)
    {
        CooldownMaxCounter[index] = newVal;
    }
    public void ChangeDeathCounterMax(float newVal)
    {
        DeathCounterMax = newVal;
    }

}
