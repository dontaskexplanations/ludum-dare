﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamepadInput;
using UnityEngine.Events;
using System;

public class InputControler : Singleton<InputControler>{
    
    private GamepadState[] _GamepadStates;
    private List<InputBehaviour>[] _InputList;

    protected InputControler(){
        _GamepadStates = new GamepadState[5];
        _InputList = new List<InputBehaviour>[5];
        _InputList[0] = new List<InputBehaviour>();
        _InputList[1] = new List<InputBehaviour>();
        _InputList[2] = new List<InputBehaviour>();
        _InputList[3] = new List<InputBehaviour>();
        _InputList[4] = new List<InputBehaviour>();
    }
    
    void Update() {
        for (int i = 0; i < 5; i++) {
            GamepadState newState = GamePad.GetState((GamePad.Index)i);

            if (newState.ThumbSticks.Left != Vector2.zero) {
                foreach (InputBehaviour inputbehaviour in _InputList[i])
                {
                    inputbehaviour.OnMove(newState.ThumbSticks.Left);
                }
            }

            if ((i == 0 && Input.GetKey(KeyCode.Space) ) || newState.A)
            {
                foreach (InputBehaviour inputbehaviour in _InputList[i])
                {
                    inputbehaviour.OnJump();
                }
            }

            if (i == 0)
            {
                Vector2 axis = new Vector2(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));

                if (axis != Vector2.zero) {
                    foreach (InputBehaviour inputbehaviour in _InputList[i])
                    {
                        inputbehaviour.OnMove(axis);
                    }
                }
                
                if (Input.GetMouseButtonDown(1)) {
                    foreach (InputBehaviour inputbehaviour in _InputList[i])
                    {
                        inputbehaviour.OnMouseLeftClick(axis);
                    }
                }
            }

            _GamepadStates[i] = newState;
        }
    }

    public void AddInput(InputBehaviour behaviour, int axis)
    {
        _InputList[axis].Add(behaviour);
    }
}
