﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour {

	// Use this for initialization

    public GameObject[] typesOfPickup;
    private GameObject pickup;
    private float _counter = 0.0f;
    public float _respawnTime = 3.0f;
    private float _offset = 0.5f;
    void Start () {
            int choice = Random.Range(0, typesOfPickup.Length);
         pickup =  Instantiate(typesOfPickup[choice],
                new Vector3(transform.position.x,transform.position.y + _offset , transform.position.z),
                Quaternion.identity);
            
    }
	
	// Update is called once per frame
	void Update () {
	    if (pickup == null && _counter >= _respawnTime)
	    {
	        int choice = Random.Range(0, typesOfPickup.Length);
	      pickup = Instantiate(typesOfPickup[choice],
	            new Vector3(transform.position.x, transform.position.y +_offset, transform.position.z),
	            Quaternion.identity);
	        _counter = 0.0f;
	    }
        else if (pickup == null )
	    {
	        _counter += Time.deltaTime;
	        if (_counter >= _respawnTime) _counter = _respawnTime;
	    }
	}
}
