﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboarding : MonoBehaviour
{

    public Camera Camera;
    private Quaternion rotation;
    // Use this for initialization
    void Start()
    {
        rotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Camera) Camera = Camera.main;
        transform.LookAt(transform.position + Camera.transform.rotation * Vector3.forward, Camera.transform.rotation * Vector3.up);
    }

    void LateUpdate()
    {
        transform.rotation = rotation;
    }
}
