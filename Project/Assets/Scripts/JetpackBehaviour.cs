﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class JetpackBehaviour : MonoBehaviour {

    public float JetpackSpeed;
    public float AirbornePlayerSpeed;
    private float _PlayerSpeed = 1;
    private float _Gravity = 1;
    public float _FuelCost = 1.0f;

    private Vector3 moveDirection = Vector3.zero;

    private Rigidbody _RB;
    private PlayerStats _PS;


    // Use this for initialization
    void Start () {
        _RB = GetComponent<Rigidbody>();
        _PS = GetComponent<PlayerStats>();
	}

    // Update is called once per frame
    void Update()
    {
       // if (isLocalPlayer)
      //  {

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);

        if (Input.GetButton("Jump") && _PS.JetPackFuel > 0f)
        {
            moveDirection.y = JetpackSpeed;
            _PS.JetPackFuel -= _FuelCost;
        }
       
            if (_RB.velocity.y < 0f)
            {
                if (_Gravity < 18) ++_Gravity;
            }
            else
            {
                _Gravity = 1;
            }

            float gravityValue = 30.0f * _Gravity;

            moveDirection.y -= gravityValue * Time.deltaTime;

            _RB.velocity = (moveDirection * _PlayerSpeed * AirbornePlayerSpeed);

       // }
    }

    float GetSpeed()
    {
        return _PlayerSpeed;
    }
    void SetSpeed(float speed)
    {
        _PlayerSpeed = speed;
    }

}
