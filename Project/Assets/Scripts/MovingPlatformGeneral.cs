﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MovingPlatformGeneral : NetworkBehaviour
{

    public GameObject[] waypoints;
    public int num = 0;

    public float minDist;
    public float speed;

    private float dist;
    private Vector3 distanceVector;
    private Vector3 normalized;
    private Rigidbody platform;

    // Use this for initialization
    void Start()
    {
        platform = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        dist = Vector3.Distance(gameObject.transform.position, waypoints[num].transform.position);
        transform.position = Vector3.MoveTowards(gameObject.transform.position, waypoints[num].transform.position, speed * Time.deltaTime);
        if (dist < minDist)
        {
            num++;
            num %= waypoints.Length;
        }
    }
}