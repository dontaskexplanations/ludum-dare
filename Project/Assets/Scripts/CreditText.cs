﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditText : MonoBehaviour 
{
    private const float maxCreditTime = 20.0f;
    private float currentTime;

    // Update is called once per frame
    void Update ()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= maxCreditTime)
        {
            currentTime = 0;
            this.gameObject.SetActive(false);
        }
    }
    
}
