﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CharacterMove : NetworkBehaviour
{
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public Vector3 moveDirection = Vector3.zero;
    private PlayerUI player_UI;
    private PlayerStats player_Stats;
    private MinionStats player_MinionStats;

    private NetworkStartPosition[] spawnPoints;

    private void Start()
    {
        if (isLocalPlayer)
        {
            //playerUI
            player_UI = GetComponent<PlayerUI>();
            player_UI.player = gameObject;
            player_UI.UI_Canvas = GameObject.Find("UI").GetComponent<Canvas>();

            //camera
            PlayerCamera cam = Camera.main.GetComponent<PlayerCamera>();
            cam.Player = gameObject;
            cam.PlayerCam = Camera.main;
            cam.Cam = cam.transform;
            cam.CharControll = gameObject;

            GunScript gunScript = cam.Gun;
            gunScript.player = gameObject;
            gunScript.playerStatistics = GetComponent<PlayerStats>();

            Billboarding billboarding = GetComponentInChildren<Billboarding>();
            billboarding.Camera = Camera.main;

            TeamComponent ok = GetComponent<TeamComponent>();
            GameObject obj = GameObject.FindGameObjectWithTag("spawn");

            if(Mathf.Abs((obj.transform.position - transform.position).magnitude) < 50)
            {
                ok.IsInBlueTeam = obj.GetComponent<TeamComponent>();
            }
            else
            {
                ok.IsInBlueTeam = !obj.GetComponent<TeamComponent>();
            }
        }
        
        player_MinionStats = GetComponent<MinionStats>();
        player_Stats = GetComponent<PlayerStats>();
        spawnPoints = FindObjectsOfType<NetworkStartPosition>();
    }

    void Update()
    {
        if (isLocalPlayer)
        {
            if (!player_Stats) player_Stats = GetComponent<PlayerStats>();
            CharacterController controller = GetComponent<CharacterController>();
            float y = moveDirection.y;

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump") && player_Stats.JetPackFuel > 0)
            {
                player_Stats.JetPackFuel--;
                y = jumpSpeed;
            }


            moveDirection.y = y;
            if (!controller.isGrounded)
                moveDirection.y -= gravity * Time.deltaTime;

            CmdMove(moveDirection);
        }
    }

    [Command]
    public void CmdMove(Vector3 moveDirection)
    {
        CharacterController controller = GetComponent<CharacterController>();
        controller.Move(moveDirection * Time.deltaTime);
        RpcMove(moveDirection);
    }

    [Command]
    public void CmdRotate(Vector3 euler)
    {
        transform.eulerAngles += euler;
    }

    [Command]
    public void CmdDealDamage(GameObject damagable, float damage)
    {
        damagable.GetComponent<MinionStats>().DealDamage(damage);
    }

    [Command]
    public void CmdBuildTower(GameObject obj)
    {
        TowerBuilder build = obj.GetComponent<TowerBuilder>();
        GameObject CreatedTower = Instantiate(build.Tower, build.transform.position, Quaternion.identity);
        NetworkServer.Spawn(CreatedTower);
        build._CreatedTower = CreatedTower;
        RpcCreatedTower(CreatedTower, obj);
        player_Stats.Money -= build.BuildCost;
        RpcMoney(player_Stats.gameObject, player_Stats.Money);
        build.IsBuildable = false;
    }

    [ClientRpc]
    public void RpcMove(Vector3 moveDirection)
    {
        CharacterController controller = GetComponent<CharacterController>();
        controller.Move(moveDirection * Time.deltaTime);
    }

    [ClientRpc]
    public void RpcCreatedTower(GameObject CreatedTower, GameObject builder)
    {
        builder.GetComponent< TowerBuilder>()._CreatedTower = CreatedTower;
    }

    [ClientRpc]
    public void RpcMoney(GameObject game, float money)
    {
        game.GetComponent<PlayerStats>().Money = money;
    }

    [Command]
    public void CmdUpgradeTower(GameObject tower)
    {
        Shop shop = tower.GetComponent<Shop>();
        player_Stats.Money -= shop._arrCosts[shop._CurrentLevel];
        shop._CurrentLevel++;
        shop._Tower.GetComponent<TowerStats>().FireInterval *= 0.75f;
        shop._Tower.GetComponent<TowerStats>().ProjectileDamage *= 1.5f;
        shop._Tower.GetComponent<SphereCollider>().radius *= 1.5f;
        RpcTower(shop._Tower.gameObject);
        
        if (shop._CurrentLevel == 4)
        {
            shop._IsUpgradable = false;
        }
    }

    [ClientRpc]
    public void RpcTower(GameObject game)
    {
        game.GetComponent<TowerStats>().FireInterval *= 0.75f;
        game.GetComponent<TowerStats>().ProjectileDamage *= 1.5f;
        game.GetComponent<SphereCollider>().radius *= 1.5f;
    }

    public void Respawn()
    {
        // Set the spawn point to origin as a default value
        GameObject spawnPoint = gameObject;

        do {
            // If there is a spawn point array and the array is not empty, pick one at random
            if (spawnPoints != null && spawnPoints.Length > 0)
            {
                spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].gameObject;
            }
        } while (spawnPoint.GetComponent<TeamComponent>().IsInBlueTeam == GetComponent<TeamComponent>().IsInBlueTeam);

        player_MinionStats.health = player_MinionStats.maxHealth;
        transform.position = spawnPoint.transform.position;
    }
}