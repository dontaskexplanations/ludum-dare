﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Networking;

public class MoneyPickup : MonoBehaviour
{
    // Use this for initialization
    public float _rotSpeed = 20.0f;
    public float _moneyAmt = 500.0f;
    public void Update()
    {
        transform.Rotate(Vector3.up, _rotSpeed * Time.deltaTime);
    }

    public void ActivatePower(PlayerStats stats)
    {
        stats.Money += _moneyAmt;
    }

    public void DestroyPowerUp()
    {
        Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            ActivatePower(col.gameObject.GetComponent<PlayerStats>());
            DestroyPowerUp();
        }
    }
}
