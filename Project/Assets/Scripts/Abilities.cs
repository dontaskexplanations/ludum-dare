﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abilities : MonoBehaviour {
    public float _JetPackCD=10 ;
    public float _BoostPower=2;

    public float _PierceCD=2;
    public float _PierceDmg=5;

    public float _InterferenceCD;

    public float _AssassinateCD=15;
    public float _KnifeDistance;
	// Use this for initialization
	void Start () {
        GameObject.Find("UI").GetComponent<UIScript>().ChangeCooldownMaxTimer(0, _JetPackCD);
        GameObject.Find("UI").GetComponent<UIScript>().ChangeCooldownMaxTimer(1, _InterferenceCD);
        GameObject.Find("UI").GetComponent<UIScript>().ChangeCooldownMaxTimer(2, _AssassinateCD);
        GameObject.Find("UI").GetComponent<UIScript>().ChangeCooldownMaxTimer(3, _PierceCD);
    }
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetButtonDown("Ability_1")) // jet boost
        {
            if (!GameObject.Find("UI").GetComponent<UIScript>().CooldownBooleans[0])
            {
                if (GetComponent<CharacterMove>().GetComponent<PlayerStats>().JetPackFuel > 50)
                {
                    GetComponent<CharacterMove>().moveDirection = new Vector3(0, _BoostPower * 15, 0);
                    GetComponent<PlayerUI>().hasBeenFlying = true;
                    GetComponent<PlayerUI>().jpTimerCounter = 0;
                    GameObject.Find("UI").GetComponent<UIScript>().CooldownBooleans[0] = true;
                    GetComponent<CharacterMove>().GetComponent<PlayerStats>().JetPackFuel -= 50;
                }
            }
        }

        if (Input.GetButtonDown("Ability_2")) // EnemyJetMalfunction
        {
            RaycastHit hit;
            if (!GameObject.Find("UI").GetComponent<UIScript>().CooldownBooleans[1])
            {
                if (GetComponent<CharacterController>().isGrounded)
                {
                    GameObject.Find("UI").GetComponent<UIScript>().CooldownBooleans[1] = true;
                    if (Physics.Raycast(GameObject.Find("PlayerCamera").GetComponent<Camera>().transform.position, GameObject.Find("PlayerCamera").GetComponent<Camera>().transform.forward, out hit))
                    {
                        //Collider target = hit.collider; // What did I hit?
                        //float distance = hit.distance; // How far out?
                        //Vector3 location = hit.point; // Where did I make impact?
                        //GameObject targetGameObject = hit.collider.gameObject; // What's the GameObject?
                        Debug.Log("test");
                        //targetGameObject.GetComponent<MinionStats>().health = 1; // temp with health to check if it works
                        GetComponent<CharacterMove>().GetComponent<PlayerStats>().JetPackFuel = 0;
                    }
                }
            }
                
        }

        if (Input.GetButtonDown("Ability_4")) // Stabbing someone
        {
            if (!GameObject.Find("UI").GetComponent<UIScript>().CooldownBooleans[2])
            {
               
                    RaycastHit hit;

                        GameObject.Find("UI").GetComponent<UIScript>().CooldownBooleans[2] = true;
                    if (Physics.Raycast(GameObject.Find("PlayerCamera").GetComponent<Camera>().transform.position, GameObject.Find("PlayerCamera").GetComponent<Camera>().transform.forward, out hit))
                    {
                     

                        if (hit.distance < _KnifeDistance)
                        {
                        // implement killing 
                        Debug.Log("kill kill kill");
                        }

                    }
                
            }
            
        }

        if (Input.GetButtonDown("Ability_3")) // PiercingSHot
        {
            if (!GameObject.Find("UI").GetComponent<UIScript>().CooldownBooleans[3])
            {
                if (GetComponent<CharacterController>().isGrounded);
                {
                    GameObject.Find("UI").GetComponent<UIScript>().CooldownBooleans[3] = true;
                    Ray theRay = new Ray(GameObject.Find("PlayerCamera").GetComponent<Camera>().transform.position, GameObject.Find("PlayerCamera").GetComponent<Camera>().transform.forward);

                    RaycastHit[] hits = Physics.RaycastAll(theRay, 500.0f);
                  
                   
                    for (int i = 0; i < hits.Length && !(i > hits.Length); i++)
                    {
                        if (hits[i].collider.CompareTag("Shark"))
                        {
                         
                            GetComponent<CharacterMove>().CmdDealDamage(hits[i].collider.gameObject, _PierceDmg);
                        }
                    }
                }
            }
        }
    }

    void RocketJump()
    {
     
    }
}
