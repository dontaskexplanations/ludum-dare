﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    public void ExitGameButton(string ExitGame)
    {
        Debug.Log("QUIT");
        Application.Quit();
    }
    public void MainMenuButton( string mainMenuScreen)
    {
        SceneManager.LoadScene(mainMenuScreen);
    }
}
